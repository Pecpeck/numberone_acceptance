import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.logging.Level;

import static org.junit.Assert.fail;

public class InscriptionPasswordTooLong {
    private HtmlUnitDriver driver;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver(true) {
            @Override
            protected WebClient newWebClient(BrowserVersion version) {
                WebClient webClient = super.newWebClient(version);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                return webClient;
            }
        };
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
    }

    @Test(timeout = 30000)
    public void testInscriptionPasswordTooLong() throws Exception {
        driver.get("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test7/preprod/index.php");
        driver.findElement(By.id("boutonInscription")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Inscription'])[1]/following::label[1]")).click();
        driver.findElement(By.id("pseudoInscription")).clear();
        driver.findElement(By.id("pseudoInscription")).sendKeys("validpseudo");
        driver.findElement(By.id("passwordInscription")).clear();
        // Password can't be longer than 51 characters.
        driver.findElement(By.id("passwordInscription")).sendKeys("Thisisaverylongpasswordverylongsoverylonglonglonglo");
        driver.findElement(By.name("action")).click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}
